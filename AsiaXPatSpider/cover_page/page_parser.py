
def parse(lines: list) -> dict:
    results = {}
    for i in range(len(lines)):
        line = lines[i]
        if len(line) >= 12 and line[:12] == "Direct Owner":
            result = {
                "title":"",
                "url":"",
                "images":[]
            }
            i2 = i + 1
            while i2 < len(lines) and not "Direct Owner" in lines[i2]:
                if len(lines[i2]) > 0 and lines[i2][0] == "[" and result["title"] == "":
                    i3 = i2
                    fulltxt = ""
                    while not lines[i3] == "":
                        fulltxt += lines[i3]
                        i3 += 1
                    result["title"] = fulltxt.split("]")[0][1:]
                    result["url"] = fulltxt.split("(")[1].split(" ")[0]
                elif len(lines[i2]) > 0 and lines[i2][0] == "*" and "image" in lines[i2]:
                    result["images"].append(lines[i2].split("(")[1].split(")")[0])
                i2 += 1
            results[result["title"]] = result
    return results