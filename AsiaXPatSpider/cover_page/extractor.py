from AsiaXPatSpider import webtools
from AsiaXPatSpider.cover_page import page_parser

def extract() -> dict:
    url = "https://hongkong.asiaxpat.com/property/direct-owner-apartments-for-rent/2/"
    lines = webtools.get_web_text(url)
    lines = webtools.select_lines(lines, "Direct Owner", "  * #### HELP & ABOUT")
    j = page_parser.parse(lines)
    print(j)