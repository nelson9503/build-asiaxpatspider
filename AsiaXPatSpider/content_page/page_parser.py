from AsiaXPatSpider import webtools

def parse(lines: list) -> dict:
    result = {
        "title":"",
        "subtitle":"",
        "price":"",
        "saleArea":"",
        "netArea":"",
        "Description":"",
        "lastUpdate":"",
        "address":"",
        "bedrooms":"",
        "bathrooms":"",
        "buildingType":"",
        "layout":"",
        "floorZone":"",
        "facilities":[],
        "views":[],
        "rooms":[],
        "contactPerson":"",
        "contactPhone":""
    }
    for i in range(len(lines)):
        line = lines[i]
        if len(line) >= 12 and line == "Direct Owner":
            result["title"] = lines[i+4]
            result["subtitle"] = lines[i+8]
        elif len(line) >= 14 and line == "Rent Per Month":
            result["price"] = line[i+2]
        elif len(line) >= 0 and line == "Description":
            i2 = i
            desc = ""
            while not "Show full" in lines[i2]:
                if not lines[i2] == "":
                    desc += lines[i2] + "\n"
                i2 += 1
        elif "Update on " in line:
            result["lastUpdate"] == line.replace("Update on ", "")
        elif "Address: " in line:
            i2 = i
            address = ""
            while not lines[i2] == "":
                address += lines[i2]
                i2 += 1
            address = address.replace("Address: ", "").split("[")[0]
            address = webtools.removeAbnormalSpaces(address)
            result["address"] = address
        elif "Bedrooms: " in line:
            result["bedrooms"] = line.replace("Bedrooms: ", "")
        elif "Bathrooms: " in line:
            result["bathrooms"] = line.replace("Bathrooms: ", "")
        elif "Building Type: " in line:
            result["buildingType"] = line.replace("Building Type: ", "")
        elif "Property Layout: " in line:
            result["layout"] = line.replace("Property Layout: ", "")
        elif "Floor Zone: " in line:
            result["floorZone"] = line.replace("Floor Zone: ", "")
        elif result["Gross Area: "] in line:
            result["netArea"] = line.replace("Gross Area: ", "")
        elif "Saleable Area: " in line:
            result["saleArea"] = line.replace("Saleable Area: ", "")
        elif line == "Facilities":
            i2 = i
            facs = []
            while not line == "* * *":
                if not line == "":
                    facs.append(line)
            result["facilities"] = facs
        elif line == "Rooms":
            i2 = i
            rooms = []
            while not line == "* * *":
                if not line == "":
                    rooms.append(line)
            result["rooms"] = rooms
        elif line == "Views":
            i2 = i
            views = []
            while not line == "* * *":
                if not line == "":
                    views.append(line)
            result["views"] = views
        elif line == "Contact Details":
            result["contactPerson"] = lines[i+2]
        elif "(tel:" in line:
            line = line.split("(tel:")[1]
            result["contactPhone"] = line.replace(")", "").replace(" ", "")
    return result