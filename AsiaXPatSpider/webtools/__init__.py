from AsiaXPatSpider.webtools.processor import (
    select_lines,
    compact_lines,
    removeAbnormalSpaces
)

from AsiaXPatSpider.webtools.requests import (
    get_web_text,
    get_web_photo
)