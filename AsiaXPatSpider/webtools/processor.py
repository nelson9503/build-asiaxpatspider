
def compact_lines(lines: list, start: str, end: str) -> list:
    newlines = []
    lock = True
    for line in lines:
        if len(line) >= len(start) and line[:len(start)] == start:
            lock = False
        if lock == True:
            continue
        if len(line) >= len(end) and line[:len(end)] == end:
            break
        line = removeAbnormalSpaces(line)
        if not line == "":
            newlines.append(line)
    return newlines


def select_lines(lines: list, start: str, end: str) -> list:
    newlines = []
    lock = True
    for line in lines:
        if len(line) >= len(start) and line[:len(start)] == start:
            lock = False
        if lock == True:
            continue
        if len(line) >= len(end) and line[:len(end)] == end:
            break
        line = removeAbnormalSpaces(line)
        newlines.append(line)
    return newlines


def removeAbnormalSpaces(x: str) -> str:
    while "  " in x:
        x = x.replace("  ", " ")
    while len(x) > 0 and x[0] == " ":
        x = x[1:]
    while len(x) > 0 and x[-1] == " ":
        x = x[:-1]
    return x
