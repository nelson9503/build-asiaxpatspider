import requests
import html2text


def get_web_text(url: str):
    r = requests.get(url)
    lines = html2text.html2text(r.text).split("\n")
    return lines


def get_web_photo(url: str, savepath: str):
    try:
        r = requests.get(url)
        with open(savepath, 'wb') as f:
            f.write(r.content)
    except:
        pass
